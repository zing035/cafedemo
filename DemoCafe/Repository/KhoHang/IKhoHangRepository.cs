﻿using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.KhoHang
{
    public interface IKhoHangRepository:IGenericRepository<Models.KhoHang>
    {
        
    }
}
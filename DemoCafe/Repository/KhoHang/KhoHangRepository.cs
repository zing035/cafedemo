﻿using DemoCafe.Models;
using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.KhoHang
{
    public class KhoHangRepository : GenericRepository<Models.KhoHang>, IKhoHangRepository
    {
        public KhoHangRepository(CafeContext context) : base(context)
        {
        }
    }
}
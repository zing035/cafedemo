﻿using DemoCafe.Models;
using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.NhanSu
{
    public class NhanSuRepository : GenericRepository<Models.NhanSu>, INhanSuRepository
    {
        public NhanSuRepository(CafeContext context) : base(context)
        {
        }
    }
}
﻿using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.NhanSu
{
    public interface INhanSuRepository : IGenericRepository<Models.NhanSu>
    {
        
    }
}
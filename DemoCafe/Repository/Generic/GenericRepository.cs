﻿using System.Collections.Generic;
using DemoCafe.Models;
using Microsoft.EntityFrameworkCore;

namespace DemoCafe.Repository.Generic
{
    public class GenericRepository<TEntyti> : IGenericRepository<TEntyti> where TEntyti : class
    {
        private readonly CafeContext _context;
        private DbSet<TEntyti> _dbSet;

        public GenericRepository(CafeContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntyti>();
        }

        public TEntyti GetById(int? id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntyti> GetAll()
        {
            return _dbSet;
        }

        public void Add(TEntyti entyti)
        {
            _dbSet.Add(entyti);
            _context.SaveChanges();
        }

        public void Update(TEntyti entyti)
        {
            _dbSet.Update(entyti);
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            TEntyti entyti = _dbSet.Find(id);
            _dbSet.Remove(entyti);
        }
    }
}
       
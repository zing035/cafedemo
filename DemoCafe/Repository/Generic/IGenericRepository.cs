﻿using System.Collections.Generic;

namespace DemoCafe.Repository.Generic
{
    public interface IGenericRepository<TEntyti>
    {
        TEntyti GetById(int? id);
        IEnumerable<TEntyti> GetAll();
        void Add(TEntyti entyti);
        void Delete(int? id);
        void Update(TEntyti entyti);
        void Save();
    }
}
﻿using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.ThuChi
{
    public interface IThuChiRepository : IGenericRepository<Models.ThuChi>
    {
        
    }
}
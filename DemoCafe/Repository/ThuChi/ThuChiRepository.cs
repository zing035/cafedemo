﻿using DemoCafe.Models;
using DemoCafe.Repository.Generic;

namespace DemoCafe.Repository.ThuChi
{
    public class ThuChiRepository : GenericRepository<Models.ThuChi>, IThuChiRepository
    {
        public ThuChiRepository(CafeContext context) : base(context)
        {
        }
    }
}
﻿using System.Collections.Generic;

namespace DemoCafe.Services.ThuChi
{
    public interface IThuChiService
    {
       Models.ThuChi GetById(int? id);
        IEnumerable<Models.ThuChi> GetAll();
        void Add(Models.ThuChi thuChi);
        void Delete(int id);
        void Update(Models.ThuChi thuChi);
        void Save();
    }
}
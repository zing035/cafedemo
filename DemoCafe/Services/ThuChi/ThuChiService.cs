﻿using System.Collections.Generic;
using DemoCafe.UnitOfWork;

namespace DemoCafe.Services.ThuChi
{
    public class ThuChiService : IThuChiService
    {
        private IUnitOfWork _unitOfWork;

        public ThuChiService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Models.ThuChi GetById(int? id)
        {
            return _unitOfWork.ThuChiRepository.GetById(id);
        }

        public IEnumerable<Models.ThuChi> GetAll()
        {
            return _unitOfWork.ThuChiRepository.GetAll();
        }

        public void Add(Models.ThuChi thuChi)
        {
            _unitOfWork.ThuChiRepository.Add(thuChi);
        }

        public void Delete(int id)
        {
            _unitOfWork.ThuChiRepository.Delete(id);
        }

        public void Update(Models.ThuChi thuChi)
        {
            _unitOfWork.ThuChiRepository.Update(thuChi);
        }

        public void Save()
        {
            _unitOfWork.ThuChiRepository.Save();
        }
    }
}
﻿using System.Collections.Generic;

namespace DemoCafe.Services.KhoHang
{
    public interface IKhoHangService
    {
       Models.KhoHang GetById(int? id);
        IEnumerable<Models.KhoHang> GetAll();
        void Add(Models.KhoHang khoHang);
        void Delete(int id);
        void Update(Models.KhoHang khoHang);
        void Save();
    }
}
﻿using System.Collections.Generic;
using DemoCafe.UnitOfWork;

namespace DemoCafe.Services.KhoHang
{
    public class KhoHangService : IKhoHangService
    {
        private IUnitOfWork _unitOfWork;

        public KhoHangService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Models.KhoHang GetById(int? id)
        {
            return _unitOfWork.KhoHangRepository.GetById(id);
        }

        public IEnumerable<Models.KhoHang> GetAll()
        {
            return _unitOfWork.KhoHangRepository.GetAll();
        }

        public void Add(Models.KhoHang khoHang)
        {
            _unitOfWork.KhoHangRepository.Add(khoHang);
        }

        public void Delete(int id)
        {
            _unitOfWork.KhoHangRepository.Delete(id);
        }

        public void Update(Models.KhoHang khoHang)
        {
            _unitOfWork.KhoHangRepository.Update(khoHang);
        }

        public void Save()
        {
            _unitOfWork.KhoHangRepository.Save();
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace DemoCafe.Services.NhanSu
{
    public interface INhanSuService
    {
        Models.NhanSu GetById(int? id);
        IEnumerable<Models.NhanSu> GetAll();
        void Add(Models.NhanSu nhanSu);
        void Delete(int id);
        void Update(Models.NhanSu nhanSu);
        void Save();
    }
}
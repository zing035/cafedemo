﻿using System.Collections.Generic;
using DemoCafe.UnitOfWork;

namespace DemoCafe.Services.NhanSu
{
    public class NhanSuService : INhanSuService
    {
        private IUnitOfWork _unitOfWork;

        public NhanSuService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Models.NhanSu GetById(int? id)
        {
            return _unitOfWork.NhanSuRepository.GetById(id);
        }

        public IEnumerable<Models.NhanSu> GetAll()
        {
            return _unitOfWork.NhanSuRepository.GetAll();
        }

        public void Add(Models.NhanSu nhanSu)
        {
           _unitOfWork.NhanSuRepository.Add(nhanSu);
        }

        public void Delete(int id)
        {
            _unitOfWork.NhanSuRepository.Delete(id);
        }

        public void Update(Models.NhanSu nhanSu)
        {
            _unitOfWork.NhanSuRepository.Update(nhanSu);
        }

        public void Save()
        {
            _unitOfWork.NhanSuRepository.Save();
        }
    }
}
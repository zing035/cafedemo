﻿using DemoCafe.Models;
using DemoCafe.Services.ThuChi;
using Microsoft.AspNetCore.Mvc;

namespace DemoCafe.Controllers
{
    public class ThuChiController : Controller
    {
        private readonly IThuChiService _thuChiService;

        public ThuChiController(IThuChiService thuChiService)
        {
            _thuChiService = thuChiService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return
            View(_thuChiService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ThuChi thuChi)
        {
            _thuChiService.Add(thuChi);
            _thuChiService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _thuChiService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(ThuChi thuChi)
        {
            _thuChiService.Update(thuChi);
            _thuChiService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _thuChiService.Delete(id);
            _thuChiService.Save();
            return RedirectToAction();
        }
    }
}
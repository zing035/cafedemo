﻿using DemoCafe.Models;
using DemoCafe.Services.KhoHang;
using Microsoft.AspNetCore.Mvc;

namespace DemoCafe.Controllers
{
    public class KhoHangController : Controller
    {
        private readonly IKhoHangService _khoHangService;

        public KhoHangController(IKhoHangService khoHangService)
        {
            _khoHangService = khoHangService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return
            View(_khoHangService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(KhoHang khoHang)
        {
            _khoHangService.Add(khoHang);
            _khoHangService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _khoHangService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(KhoHang khoHang)
        {
            _khoHangService.Update(khoHang);
            _khoHangService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _khoHangService.Delete(id);
            _khoHangService.Save();
            return RedirectToAction("Index");
        }
    }
}
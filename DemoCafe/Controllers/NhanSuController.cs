﻿using DemoCafe.Models;
using DemoCafe.Services.NhanSu;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;

namespace DemoCafe.Controllers
{
    public class NhanSuController : Controller
    {
        private readonly INhanSuService _nhanSuService;

        public NhanSuController(INhanSuService nhanSuService)
        {
            _nhanSuService = nhanSuService;
        }
        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return
            View(_nhanSuService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(NhanSu nhanSu)
        {
            _nhanSuService.Add(nhanSu);
            _nhanSuService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _nhanSuService.GetById(id);
            return View(getId);
        }
        [HttpPost]
        public IActionResult Edit(NhanSu nhanSu)
        {
            _nhanSuService.Update(nhanSu);
            _nhanSuService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _nhanSuService.Delete(id);
            _nhanSuService.Save();
            return RedirectToAction("Index");
        }
    }
}
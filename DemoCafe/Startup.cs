﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DemoCafe.Models;
using DemoCafe.Repository.Generic;
using DemoCafe.Repository.KhoHang;
using DemoCafe.Repository.NhanSu;
using DemoCafe.Repository.ThuChi;
using DemoCafe.Services.KhoHang;
using DemoCafe.Services.NhanSu;
using DemoCafe.Services.ThuChi;
using DemoCafe.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DemoCafe
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddTransient<IGenericRepository<NhanSu>, GenericRepository<NhanSu>>();
            services.AddTransient<IGenericRepository<KhoHang>, GenericRepository<KhoHang>>();
            services.AddTransient<IGenericRepository<ThuChi>, GenericRepository<ThuChi>>();
            services.AddTransient<INhanSuService, NhanSuService>();
            services.AddTransient<IKhoHangService, KhoHangService>();
            services.AddTransient<IThuChiService, ThuChiService>();
            services.AddTransient<IUnitOfWork, UnitOfWork.UnitOfWork>();
            services.AddTransient<INhanSuRepository, NhanSuRepository>();
            services.AddTransient<IKhoHangRepository, KhoHangRepository>();
            services.AddTransient<IThuChiRepository, ThuChiRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<CafeContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DemoCafeConnectionString")));
            
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
﻿using System;
using DemoCafe.Models;
using DemoCafe.Repository.Generic;

namespace DemoCafe.UnitOfWork
{
    public class UnitOfWork :IUnitOfWork
    {
        private CafeContext _context;

        public UnitOfWork(CafeContext context)
        {
            _context = context;
            InitRepository();
        }

        private void InitRepository()
        {
            NhanSuRepository=new GenericRepository<NhanSu>(_context);
            KhoHangRepository=new GenericRepository<KhoHang>(_context);
            ThuChiRepository=new GenericRepository<ThuChi>(_context);
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private IGenericRepository<NhanSu> NhanSuRepository { get; set; }

        IGenericRepository<NhanSu> IUnitOfWork.NhanSuRepository
        {
            get { return this.NhanSuRepository; }
            set { this.NhanSuRepository = value; }
        }

        private IGenericRepository<KhoHang> KhoHangRepository { get; set; }

        IGenericRepository<KhoHang> IUnitOfWork.KhoHangRepository
        {
            get { return this.KhoHangRepository; }
            set { this.KhoHangRepository = value; }
        }

        private IGenericRepository<ThuChi> ThuChiRepository { get; set; }

        IGenericRepository<ThuChi> IUnitOfWork.ThuChiRepository
        {
            get { return this.ThuChiRepository; }
            set { this.ThuChiRepository = value; }
        }

        public void Save()

        {
            _context.SaveChanges();
        }
    }
}
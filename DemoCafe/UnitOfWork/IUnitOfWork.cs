﻿using System;
using DemoCafe.Models;
using DemoCafe.Repository.Generic;

namespace DemoCafe.UnitOfWork
{
    public interface IUnitOfWork :IDisposable
    {
        IGenericRepository<NhanSu> NhanSuRepository { get; set; }
        IGenericRepository<KhoHang>KhoHangRepository { get; set; }
        IGenericRepository<ThuChi>ThuChiRepository { get; set; }
        void Save();
    }
}
﻿
using System.ComponentModel.DataAnnotations;

namespace DemoCafe.Models
{
    public class ThuChi
    {
        [Key]
        public int IdMaDonHang { get; set; }
        public string TenHang { get; set; }
        public string SoLuong { get; set; }
        public float DoanhThu { get; set; }
        public float ChiTieu { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace DemoCafe.Models
{
    public class NhanSu
    {
        [Key]
        public int IdMaNhanVien { get; set; }
        public string FulllName { get; set; }
        public string Sdt { get; set; }
    }
}
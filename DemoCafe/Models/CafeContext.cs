﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DemoCafe.Models
{
    public class CafeContext : DbContext
    {
        public CafeContext(DbContextOptions<CafeContext> options) : base(options)

        {
        }

        public DbSet<NhanSu> NhanSus { get; set; }
        public DbSet<KhoHang> KhoHangs { get; set; }
        public DbSet<ThuChi>ThuChis { get; set; }
    }
}
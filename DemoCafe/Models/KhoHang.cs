﻿
using System.ComponentModel.DataAnnotations;

namespace DemoCafe.Models
{
    public class KhoHang
    {
        [Key]
        public int IdStt { get; set; }
        public string NLPhaChe { get; set; }
        public string DungCu { get; set; }
    }
}